#!/usr/bin/env python

from dataclasses import dataclass
from email.header import Header
from email.mime.text import MIMEText
from email.utils import formatdate, make_msgid
from random import shuffle
from smtplib import SMTP

from jinja2 import Template


@dataclass(frozen=True)
class Person:
    username: str
    additional_information: str
    email: str


def load_people() -> list[Person]:
    """
    Load people from CSV
    """
    people = []

    with open('secret_santa.csv') as f:
        for line in f:
            line = line.replace('\n', '')
            line = line.replace('"', '')
            sp = line.split(',')
            sp = (s.strip() for s in sp)
            person = Person(*sp)
            people.append(person)

    return people


def shuffle_people(people: list[Person]) -> list[Person]:
    """
    Take random permutation of people,
    and ensure this permutation has no single point.

    This seems to be not efficient, but the expected number
    of generated permutations is e < 3, which is bounded.
    This is so satisfying.
    """
    shuffled = people.copy()

    while True:
        shuffle(shuffled)
        for p1, p2 in zip(people, shuffled):
            if p1 == p2:
                break
        else:
            return shuffled


def main() -> None:
    people = load_people()
    shuffled = shuffle_people(people)
    myna = Person("Mina","Magicarpe","myna@crans.org")
    people.append(myna)
    suffled.append(myna)

    with open('output', 'w') as f:
        for p1, p2 in zip(people, shuffled):
            print(p1.username, p2.username, sep='\t', file=f)
            print(p1.username, p2.username, sep='\t')

    with open('secret_santa.j2') as f:
        mail_template = f.read()

    mail_template = Template(mail_template)

    from_header = Header("Le Père Noël")
    from_header.append("<med@crans.org>")

    with SMTP('smtp.crans.org') as smtp:
        for p1, p2 in zip(people, shuffled):
            content = mail_template.render(p1=p1, p2=p2)

            mail = MIMEText(content, _charset='utf-8')
            mail['From'] = from_header
            to_header = Header(p1.username)
            to_header.append(f"<{p1.email}>")
            mail['To'] = to_header
            mail['Subject'] = "Père Noël secret de la Med"
            mail['Date'] = formatdate(localtime=True)
            mail['Message-ID'] = make_msgid(domain='crans.org')

            smtp.sendmail('med@crans.org', [p1.email], mail.as_string())


if __name__ == '__main__':
    main()
